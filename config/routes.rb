Rails.application.routes.draw do
  resources :tweets
  root 'tweets#index'
  root 'tweets#new'
end
